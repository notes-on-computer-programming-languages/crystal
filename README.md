# crystal

Inspirated by Ruby, garbage collected, compiled language. https://crystal-lang.org

![Crystal popularity on Debian](https://qa.debian.org/cgi-bin/popcon-png?packages=crystal+elixir&show_installed=1&show_vote=0&show_old=0&show_recent=0&show_nofiles=0&want_percent=0&want_legend=1&want_ticks=1&from_date=2014-01-01&to_date=&hlght_date=&date_fmt=%25Y)

# Books
* *Crystal Programming*
  2022 Guilherme Bernal, George Dietrich (Packt Publishing)
* *Programming Crystal*
  2019 Ivo Balbaert, Simon St. Laurent (Pragmatic Bookshelf)

# Influenced by
* [*Ruby*
  ](https://en.m.wikipedia.org/wiki/Ruby_(programming_language))
  (2019)

# Unofficial documentation
* [*Crystal*
  ](https://en.m.wikipedia.org/wiki/Crystal_(programming_language))
* [*Why you should switch from Ruby to Crystal*
  ](https://blog.logrocket.com/why-you-should-switch-from-ruby-to-crystal/)
  2020-10 Shadid Haque @LogRocket
* [*Awesome Crystal*
  ](https://github.com/veelenga/awesome-crystal)
  (2019)
